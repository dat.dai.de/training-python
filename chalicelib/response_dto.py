import json
import datetime


class ResponseDTO:
    def __init__(self, status, message, data):
        self.status = status
        self.message = message
        self.data = data

    def toJson(self):
        return json.dumps(self, default=myconverter)


def myconverter(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()
        else:
            return o.__dict__