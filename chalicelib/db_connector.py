import mysql.connector

mydb = mysql.connector.connect(user='root', password='admin',
                               host='127.0.0.1', database='training',
                               auth_plugin='mysql_native_password')
mycursor = mydb.cursor(buffered=True, dictionary=True)


def find_by_Id(userId):
    query = "select * from user where id = %s"
    val = (userId,)
    mycursor.execute(query, val)
    return mycursor.fetchone()


def find_all():
    query = "select * from user"
    mycursor.execute(query)
    return mycursor.fetchall()


def find_by_username(username):
    query = "select * from user where username = %s"
    val = (username,)
    mycursor.execute(query, val)
    return mycursor.fetchone()


def find_by_email(username):
    query = "select * from user where email = %s"
    val = (username,)
    mycursor.execute(query, val)
    return mycursor.fetchone()


def create_new_user(user):
    query = "insert into user(username, password, full_name, email, dob, is_admin) value (%s, %s, %s, %s, %s, %s)"
    val = (user.get('username'), user.get('password'), user.get('full_name'), user.get('email'), user.get('dob'),
           user.get('is_admin'))
    mycursor.execute(query, val)
    mydb.commit()


def update_user(user, user_id):
    query = "update user set username = %s, password = %s, full_name = %s, email = %s, dob = %s, is_admin = %s where id = %s"
    val = (user.get('username'), user.get('password'), user.get('full_name'), user.get('email'), user.get('dob'),
           user.get('is_admin'), user_id)
    mycursor.execute(query, val)
    mydb.commit()


def delete_user(user_id):
    query = "delete from user where id = %s"
    val = (user_id,)
    mycursor.execute(query, val)
    mydb.commit()
