import datetime

import jwt

_SECRET = b'\xf7\xb6k\xabP\xce\xc1\xaf\xad\x86\xcf\x84\x02\x80\xa0\xe0'


def get_jwt_token(id):
    payload = {
        'id': id,
        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=180000000)
        # NOTE: We can also add 'exp' if we want tokens to expire.
    }
    return jwt.encode(payload, _SECRET, algorithm='HS256')


def decode_jwt_token(token):
    return jwt.decode(token, _SECRET, algorithms=['HS256'])
