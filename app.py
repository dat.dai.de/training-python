import datetime
import json
from validate_email import validate_email

from chalice import Chalice, AuthResponse, Response
import bcrypt
from jwt import ExpiredSignatureError

from chalicelib import auth, db_connector, response_dto

app = Chalice(app_name='training')


@app.authorizer()
def jwt_auth(auth_request):
    token = auth_request.token
    if token.startswith('Bearer '):
        token = token.replace('Bearer ', '', 1)
    try:
        decoded = auth.decode_jwt_token(token)
    except ExpiredSignatureError:
        return AuthResponse(routes=['e'], principal_id='')
    return AuthResponse(routes=['*'], principal_id=decoded['id'])


def admin(f):
    def func(*args, **kwargs):
        user_id = getAuthorizedId(app.current_request)
        user = db_connector.find_by_Id(user_id)
        if user.get("is_admin") != 1:
            return createResponse(401, 'Access deny', None, {})
        return f(*args, **kwargs)

    return func


def validate_exist_username(username):
    if db_connector.find_by_username(username) is not None:
        raise ValueError('{} is exist!', username)


def validate_exist_email(email):
    if db_connector.find_by_email(email) is not None:
        raise ValueError('{} is used!', email)


@app.route('/create-user', authorizer=jwt_auth, methods=['POST'])
@admin
def create_new_user():
    body = app.current_request.json_body
    try:
        validate(username=body.get('username'), password=body.get('password'), email=body.get('email'),
                 is_admin=body.get('is_admin'))
        if body.get('dob') is not None:
            validate(dob=body.get('dob'))
        validate_exist_username(body.get('username'))
        validate_exist_email(body.get('email'))
    except ValueError as e:
        return createResponse(400, str(e), None, {})
    hashed = bcrypt.hashpw(bytes(body['password'], 'utf-8'), bcrypt.gensalt(10, b'2a'))
    body['password'] = hashed.decode('utf-8')
    db_connector.create_new_user(body)
    return createResponse(200, 'Success', None, {})


@app.route('/update-user', authorizer=jwt_auth, methods=['PUT'])
def update_user():
    body = app.current_request.json_body
    if body.get('id') is None or (type(body.get('id')) is str and not body.get('id').isdigit()) :
        body['id'] = getAuthorizedId(app.current_request)
    try:
        validate(username=body.get('username'), email=body.get('email'),
                 is_admin=body.get('is_admin'))
        if body.get('dob') is not None:
            validate(dob=body.get('dob'))
    except ValueError as e:
        return createResponse(400, str(e), None, {})
    user = db_connector.find_by_Id(body.get('id'))
    body['password'] = user.get('password')
    db_connector.update_user(body, body.get('id'))
    return createResponse(200, 'Success', None, {})


@app.route('/delete-user/{user_id}', authorizer=jwt_auth, methods=['DELETE'])
@admin
def delete_user(user_id):
    if not user_id.isdigit():
        user_id = getAuthorizedId(app.current_request)
    db_connector.delete_user(user_id)
    return createResponse(200, 'Success', None, {})


@app.route('/get-user/{user_id}', authorizer=jwt_auth)
def get_user(user_id):
    if user_id == '0' or not user_id.isdigit():
        user_id = getAuthorizedId(app.current_request)
    return createResponse(200, 'Success', db_connector.find_by_Id(user_id), {})


@app.route('/get-all-user', authorizer=jwt_auth)
@admin
def get_all_user():
    return createResponse(200, 'Success', db_connector.find_all(), {})


@app.route('/login', methods=['POST'])
def login():
    body = app.current_request.json_body
    try:
        validate(username=body.get('username'), password=body.get('password'))
    except ValueError as e:
        return createResponse(400, str(e), None, {})
    user = db_connector.find_by_username(body['username'])
    if user is None:
        return createResponse(400, 'Invalid username or password', None, {})
    password = user['password']
    if not bcrypt.checkpw(bytes(body['password'], 'utf-8'), bytes(password, 'utf-8')):
        return createResponse(400, 'Invalid username or password', None, {})
    jwtToken = auth.get_jwt_token(user['id'])
    return createResponse(200, 'Ok', user, {'Authorization': jwtToken.decode('utf-8')})


def createResponse(status, msg, data, customHeader):
    commonHeader = {'Content-Type': 'application/json'}
    body = response_dto.ResponseDTO(status, msg, data)
    headers = {**commonHeader, **customHeader}
    return Response(body=body.toJson(), status_code=status, headers=headers)


def getAuthorizedId(currentRequest):
    return currentRequest.context['authorizer']['principalId']


def user_validate(f):
    def func(**kwargs):
        for key, value in kwargs.items():
            if value is None or value == '':
                raise ValueError('{}: is empty!'.format(key))
            if key == 'email' and not validate_email(value):
                raise ValueError('Email is not valid!')
            if key == 'dob' and not is_valid_date(value):
                raise ValueError('Date of birth is not valid!')
            if key == 'is_admin' and (value != '1' and value != '0'):
                raise ValueError('is_admin is not valid!')
        return f(**kwargs)

    return func


@user_validate
def validate(**kwargs):
    pass


def is_valid_date(date_text):
    try:
        datetime.datetime.strptime(date_text, '%Y-%m-%d')
        return True
    except ValueError:
        return False
